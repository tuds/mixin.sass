var gulp = require('gulp');
var gulp_wait = require('gulp-wait');
var sass = require('gulp-sass');

var jsmin = require('gulp-jsmin');
var cssmin = require('gulp-cssmin');

var gulp_file_include = require('gulp-file-include');
var browser_sync = require('browser-sync').create();

// SYNC sass
gulp.task('sass', function(){
    gulp.src(['./sass/amp-scss/*.scss'])
    .pipe(sass())
    .pipe(cssmin())
    .pipe(gulp.dest('./html/html-style'))
    .pipe(browser_sync.stream());
});

// SYNC html
gulp.task('include-html', function(){
    gulp.src([
        '!./html/html-global/*.html',
        '!./html/docs/*.html',
        './html/*.html'
    ])
    .pipe(gulp_wait(500))
    .pipe(gulp_file_include())
    .pipe(gulp.dest('./public'))
    .pipe(browser_sync.stream());
});

// SYNC js
gulp.task('js', function(){
    gulp.src([
        './theme/js/*.js'
    ])
    .pipe(gulp.dest('./public/theme/js'));
});

// COPY images
gulp.task('copy-img', function(){
    gulp.src([
        './theme/images/**/*.png',
        './theme/images/**/*.jpg',
        './theme/images/**/*.gif',
        './theme/images/**/*.svg'
    ])
    .pipe(gulp.dest('./public/theme/images'));
});

gulp.task("Sync", ['include-html', 'sass', 'js'], function(){
    browser_sync.init({
        server: {
            baseDir: "./public"
        }
    });
    gulp.watch(['./html/**/*.html', './html/html-style/*.css'], ['include-html']);
    gulp.watch(['./sass/**/*.scss'], ['sass']);
})

gulp.task('default', ['Sync', 'copy-img']);
